-module(hfe_string_io).

-export([to_file/2, init/2]).

%% @doc
%%  Creates a filedescriptor from a string. Then we can pass it around to any
%%  function of the `file` or `io` module where an io_device() is needed.
to_file(String, Opts) ->
  {ok, spawn(?MODULE, init, [String, Opts])}.




init(String, Opts) ->
  {ok, FD} = file:open(String, [ram|Opts]),
  loop(FD).

loop(FD) ->
  receive
    {file_request, From, ReplyAs, {pread, Pos, Len}} ->
      Result = file:pread(FD, Pos, Len),
      From ! {file_reply, ReplyAs, Result},
      loop(FD);
    {file_request, From, ReplyAs, close} ->
      Result = file:close(FD),
      From ! {file_reply, ReplyAs, Result};
    CatchAll ->
      io:format("Received something to catch all: ~p~n", [CatchAll])
  end.


