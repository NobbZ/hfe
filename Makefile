UNICODE_VERSION = 10.0.0

ERL_SOURCES=$(wildcard src/*.erl) src/hfe_unicode.erl
ERL_HEADERS=$(wildcard include/*.hrl)

GEN_DIRS=_build
GEN_FILES=priv/UnicodeData.txt src/hfe_unicode.erl

REBAR=rebar3

.PHONY: all eunit proper test

all: default_compile

test: test_compile eunit proper dialyzer

eunit: 
	${REBAR} eunit

proper:
	${REBAR} proper

dialyzer:
	${REBAR} dialyzer

cloc:
	cloc --exclude-dir=_build .

compile: ${ERL_HEADERS} ${ERL_SOURCES}

clean: $(GEN_DIRS:%=%_dir_clean) $(GEN_FILES:%=%_clean)

%_dir_clean:
	rm -rf $*

%_clean:
	rm -f $*

%_compile: compile
	${REBAR} as $* compile

src/hfe_unicode.erl: priv/UnicodeData.txt priv/generate_unicode_module.erl
	./priv/generate_unicode_module.erl $@ $<

priv/UnicodeData.txt:
	cd priv/ && wget ftp://ftp.unicode.org/Public/${UNICODE_VERSION}/ucd/UnicodeData.txt
