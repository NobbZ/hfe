%% @doc
%%  The functions in this module are used to process a string or a file and turn
%%  it into a list of tokens.
%%  It is loosely based on golangs template lexer.
%% @reference <a href="https://github.com/golang/go/blob/master/src/text/template/parse/lex.go">Source Code of the go template lexer</a>
%% @author Norbert Melzer
-module(hfe_lexer).

-author("Norbert Melzer").

-behaviour(gen_statem).

-include("token.hrl").

-export([tokenize/1]).

-export([init/1, handle_event/4, terminate/3, callback_mode/0]).

-define(is_digit(C), ($0 =< C andalso C =< $9)).
-define(is_lower(C), ($a =< C andalso C =< $z)).
-define(is_upper(C), ($A =< C andalso C =< $Z)).
-define(is_alpha(C), (?is_lower(C) orelse ?is_upper(C))).
-define(is_alnum(C), (?is_alpha(C) orelse ?is_digit(C))).
-define(is_symbl(C), (C =:= $! orelse C =:= $/ orelse C =:= $\\ orelse C =:= $& orelse C =:= $% orelse C =:= $$ orelse C =:= $= orelse C =:= $? orelse C =:= $* orelse C =:= $+ orelse C =:= $# orelse C =:= $- orelse C =:= $_ orelse C =:= $< orelse C =:= $> orelse C =:= $|)).

-record(lexer, {
  name     = <<"<input>">>,
  input    = undefined,
  pos      = 0,
  start    = 0,
  receiver = undefined,
  stack    = [],
  line     = 1,
  type     = undefined
}).

-type stream_pos() :: non_neg_integer().

-type lexer() :: #lexer{
  name     :: binary(),
  input    :: undefined | file:io_device(),
  pos      :: stream_pos(),
  start    :: stream_pos(),
  receiver :: undefined | pid(),
  stack    :: list(atom()),
  line     :: pos_integer(),
  type     :: undefined | token_type()
}.

%% @doc
%%  Splits the given binary and returns a list of tokens.
%% @param Input The string to tokenize.
%% @returns A list of tokens
-spec tokenize(Input :: string()) -> [].
tokenize(String) ->
  {ok, FD} = hfe_string_io:to_file(String, [read, binary]),
  {ok, PID} = tokenize(FD, self()),
  Ref = monitor(process, PID),
  gen_statem:cast(PID, start),
  Tokens = lists:reverse(tokenize_loop(Ref, [])),
  ok = file:close(FD),
  Tokens.

-spec tokenize_loop(reference(), list()) -> list().
tokenize_loop(Ref, Acc) ->
  receive
    {'DOWN', Ref, _, _, _} -> Acc;
    {'DOWN', _, _, _, _} -> tokenize_loop(Ref, Acc);
    Token when is_tuple(Token) ->
      tokenize_loop(Ref, [Token|Acc]);
    eof -> Acc;
    X -> io:format("~p~n", [X])
  end.

-spec tokenize(file:io_device(), pid()) -> {ok, pid()} | no_return().
tokenize(FD, Receiver) ->
  Lexer = #lexer{input = FD, receiver = Receiver},
  {ok, _} = gen_statem:start(?MODULE, Lexer, []).



-spec callback_mode() -> handle_event_function.
callback_mode() ->
    handle_event_function.

-spec init(lexer()) -> {ok, token_start, lexer()}.
init(Lexer = #lexer{}) ->
  {ok, token_start, Lexer}.

-spec handle_event(gen_statem:event_type(), atom(), token_start | token_type(), lexer()) -> gen_statem:event_handler_result(lexer()).
handle_event(cast, start, token_start, Lexer) ->
  {next_state, token_start, Lexer, {next_event, internal, reset}};
handle_event(internal, emit, token_start, Lexer) ->
  emit(Lexer),
  {next_state, token_start, Lexer, {next_event, internal, reset}};
handle_event(internal, reset, token_start, Lexer) ->
  Lexer1 = Lexer#lexer{start = Lexer#lexer.pos, type = undefined},
  {next_state, token_start, Lexer1, {next_event, internal, choose}};
handle_event(internal, choose, token_start, Lexer) ->
  case peek(Lexer) of
    C when ?is_digit(C) ->
      {next_state, integer, Lexer, {next_event, internal, continue}};
    C when ?is_lower(C) ->
      {next_state, lower_id, Lexer, {next_event, internal, continue}};
    C when ?is_upper(C) ->
      {next_state, upper_id, Lexer, {next_event, internal, continue}};
    C when ?is_symbl(C) ->
      {next_state, operator, Lexer, {next_event, internal, continue}};
    $( ->
      {next_state, '(', Lexer, {next_event, internal, continue}};
    $) ->
      {next_state, ')', Lexer, {next_event, internal, continue}};
    ${ ->
      {next_state, '{', Lexer, {next_event, internal, continue}};
    $} ->
      {next_state, '}', Lexer, {next_event, internal, continue}};
    $[ ->
      {next_state, '[', Lexer, {next_event, internal, continue}};
    $] ->
      {next_state, ']', Lexer, {next_event, internal, continue}};
    error ->
      {next_state, eof, Lexer, {next_event, internal, eof}};
    _ ->
      {next_state, unknown, advance(Lexer, 1), {next_event, internal, emit}}
  end;

handle_event(internal, continue, integer, Lexer) ->
  Lexer1 = take_while(fun(C) -> ?is_digit(C) end, Lexer),
  case peek(Lexer1) of
    $. -> {next_state, float,   Lexer1, {next_event, internal, dot}};
    _  -> {next_state, integer, Lexer1, {next_event, internal, emit}}
  end;

handle_event(internal, continue, ID, Lexer) when ID =:= lower_id; ID =:= upper_id ->
  Lexer1 = advance(Lexer, 1),
  Lexer2 = take_while(fun(C) -> ?is_alnum(C) orelse C =:= $' end, Lexer1),
  {next_state, ID, Lexer2, {next_event, internal, emit}};

handle_event(internal, continue, P, Lexer) when P =:= '('; P =:= ')'; P =:= '{'; P =:= '}'; P =:= '['; P =:= ']' ->
  Lexer1 = advance(Lexer, 1), % Consume the parenthesis/bracket
  {next_state, P, Lexer1, {next_event, internal, emit}};

handle_event(internal, dot, float, Lexer) ->
  Lexer1 = advance(Lexer, 1), % Consume the dot
  Lexer2 = take_while(fun(C) -> ?is_digit(C) end, Lexer1),
  case peek(Lexer2) of
    $e -> {next_state, float, Lexer2, {next_event, internal, e}};
    _  -> {next_state, float, Lexer2, {next_event, internal, emit}}
  end;
handle_event(internal, e, float, Lexer) ->
  Lexer1 = advance(Lexer, 1), % Consume the e
  Lexer2 = case peek(Lexer1) of
    $+ -> advance(Lexer1, 1);
    $- -> advance(Lexer1, 1);
    _  -> Lexer1
  end,
  Lexer3 = take_while(fun(C) -> ?is_digit(C) end, Lexer2),
  {next_state, float, Lexer3, {next_event, internal, emit}};

handle_event(internal, continue, operator, Lexer) ->
  Lexer1 = take_while(fun(C) -> ?is_symbl(C) end, Lexer),
  {next_state, operator, Lexer1, {next_event, internal, emit}};

handle_event(internal, emit, TokenType, Lexer) ->
  {next_state, token_start, Lexer#lexer{type = TokenType}, {next_event, internal, emit}};

handle_event(internal, eof, eof, Lexer) ->
  eof(Lexer),
  {stop, {shutdown, eof}, Lexer};

handle_event(A, B, C, D) ->
  io:format("~p, ~p, ~p, ~p~n", [A, B, C, D]),
  stop.

terminate({shutdown, eof}, eof, _) -> ok;
terminate(R, S, D) ->
  io:format("Terminate because ~p from state ~p while ~p~n", [R, S, D]).



-spec eof(lexer()) -> eof.
eof(Lexer) ->
  Lexer#lexer.receiver ! eof.

-spec emit(lexer()) -> token().
emit(Lexer) ->
  Start       = Lexer#lexer.start,
  Len         = Lexer#lexer.pos - Start,
  {ok, Value} = file:pread(Lexer#lexer.input, Start, Len),
  Lexer#lexer.receiver ! #token{type = Lexer#lexer.type, value = Value}.

-spec peek(lexer()) -> char() | error.
peek(Lexer) ->
  {C, _} = do_peek(Lexer),
  C.

-spec do_peek(lexer()) -> {error, 0} | {char(), 1..4}.
do_peek(Lexer) ->
  Bytes = file:pread(Lexer#lexer.input, Lexer#lexer.pos, 4),
  case Bytes of
    eof ->
      {error, 0};
    {error, _} ->
      {error, 0};
    {ok, <<2#0:1,     C:7,                              _/binary>>} ->
      {C, 1};
    {ok, <<2#10:2, _/bitstring>>} ->
      {error, 0};
    {ok, <<2#110:3,   C1:5, 2#10:2, C2:6,               _/binary>>} ->
      <<C:11>> = <<C1:5, C2:6>>,
      {C, 2};
    {ok, <<2#1110:4,  C1:4, 2#10:2, C2:6, 2#10:2, C3:6, _/binary>>} ->
      <<C:16>> = <<C1:4, C2:6, C3:6>>,
      {C, 3};
    {ok, <<2#11110:5, C1:3, 2#10:2, C2:6, 2#10:2, C3:6, 2#10:2, C4:6>>} ->
      <<C:21>> = <<C1:3, C2:6, C3:6, C4:6>>,
      {C, 4}
  end.

-spec take_while(fun((char()) -> boolean()), lexer()) -> lexer().
take_while(P, Lexer) ->
  take_while(Lexer, P, Lexer#lexer.pos, 0).

-spec take_while(lexer(), fun((char()) -> boolean()), stream_pos(), stream_pos()) -> lexer().
take_while(Lexer, P, Pos, Len) ->
  case do_peek(Lexer) of
    {error, _} ->
      Lexer;
    {C, B} ->
      case P(C) of
        false ->
          Lexer;
        true ->
          NewLexer = advance(Lexer, B),
          take_while(NewLexer, P, Pos, Len + B)
      end
  end.

-spec advance(lexer(), non_neg_integer()) -> lexer().
advance(Lexer, Bytes) ->
  Lexer#lexer{pos = Lexer#lexer.pos + Bytes}.
