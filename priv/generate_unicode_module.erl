#!/usr/bin/env escript

-record(ucd, {
  cp, name, gen_cat, can_com_class, bidi_class, decomp_type, num_type, bidi_mirror, up_map, low_map, title_map
}).

-include_lib("kernel/include/file.hrl").

main([]) ->
  Src = "priv/UnicodeData.txt",
  Dest = "src/hfe_unicode.erl",
  MTimeSrc = download(Src),

  io:format("Checking for ~p~n", [Dest]),
  case file:read_file_info(Dest, [{time, posix}]) of
    {ok, #file_info{mtime = MTimeDest}} when MTimeDest > MTimeSrc ->
      io:format("Is current.~n");
    _ ->
      io:format("Reading file~n"),
      Lines = read(Src),
      io:format("Parsing records~n"),
      Records = lists:map(fun (L) -> parse_line(L) end, Lines),

      io:format("Generating module~n"),
      ModuleName = filename:basename(Dest, ".erl"),
      ModuleAST = gen_module(ModuleName, Records),

      io:format("Writing file~n"),
      {ok, Fd} = file:open(Dest, [write]),
      io:format(Fd, "~s~n", [erl_prettypr:format(ModuleAST)]),
      file:close(Fd)
  end.

download(Src) ->
  io:format("Checking for ~s~n", [Src]),
  case file:read_file_info(Src, [{time, posix}]) of
    {error, _} ->
      io:format("Connecting to ftp.unicode.org~n"),
      {ok, PID} = ftp:open("ftp.unicode.org"),
      ok = ftp:user(PID, "anonymous", "anonymous"),
      io:format("Changing folder on the server~n"),
      ok = ftp:cd(PID, "/Public/10.0.0/ucd"),
      io:format("Changing folder locally~n"),
      ok = ftp:lcd(PID, "priv"),
      io:format("Downloading file~n"),
      ok = ftp:recv(PID, "UnicodeData.txt"),
      io:format("Download finished~n"),
      case file:read_file_info(Src, [{time, posix}]) of
        {ok, #file_info{mtime = MT}} -> MT
      end;
    {ok, FI} ->
      io:format("File exists.~n"),
      FI#file_info.mtime
  end.

read(FileName) ->
  {ok, Fd} = file:open(FileName, [read]),
  read(Fd, []).

read(Fd, Acc) ->
  case file:read_line(Fd) of
    {ok, Line} -> read(Fd, [Line|Acc]);
    eof -> lists:reverse(Acc)
  end.

parse_line(Line) ->
  Line1 = string:strip(Line, both, $\n),
  [CP, Name, GenCat, CanComClass, BiDiClass, DecompType, NumType, BidiMirror, _, _, UpMap, LowMap, TitleMap, _, _] =
    string:split(Line1, ";", all),
  #ucd{
    cp = list_to_integer(CP, 16),
    name = Name,
    gen_cat = GenCat,
    can_com_class = CanComClass,
    bidi_class = BiDiClass,
    decomp_type = DecompType,
    num_type = NumType,
    bidi_mirror = BidiMirror,
    up_map = UpMap,
    low_map = LowMap,
    title_map = TitleMap
  }.

gen_module(ModuleName, UCD) ->
  erl_syntax:form_list([
    erl_syntax:attribute(
      erl_syntax:atom("module"), [erl_syntax:atom(ModuleName)]),

    erl_syntax:attribute(
      erl_syntax:atom("export"), [erl_syntax:list([
        erl_syntax:arity_qualifier(erl_syntax:atom("is_letter"), erl_syntax:integer(1)),
        erl_syntax:arity_qualifier(erl_syntax:atom("is_upper"), erl_syntax:integer(1)),
        erl_syntax:arity_qualifier(erl_syntax:atom("is_lower"), erl_syntax:integer(1)),
        erl_syntax:arity_qualifier(erl_syntax:atom("is_digit"), erl_syntax:integer(1))])]),

    erl_syntax:function(
      erl_syntax:atom("is_letter"),
        lists:filtermap(fun (R) -> is_letter_clause(R) end, UCD) ++ [erl_syntax:clause([erl_syntax:underscore()], none, [erl_syntax:atom("false")])]),

    erl_syntax:function(
      erl_syntax:atom("is_upper"),
        lists:filtermap(fun (R) -> is_upper_clause(R) end, UCD) ++ [erl_syntax:clause([erl_syntax:underscore()], none, [erl_syntax:atom("false")])]),

    erl_syntax:function(
      erl_syntax:atom("is_lower"),
        lists:filtermap(fun (R) -> is_lower_clause(R) end, UCD) ++ [erl_syntax:clause([erl_syntax:underscore()], none, [erl_syntax:atom("false")])]),

    erl_syntax:function(
      erl_syntax:atom("is_digit"),
        lists:filtermap(fun (R) -> is_digit_clause(R) end, UCD) ++ [erl_syntax:clause([erl_syntax:underscore()], none,  [erl_syntax:atom("false")])])
  ]).

is_letter_clause(#ucd{cp = CP, gen_cat = [$L|_]}) ->
  {true, erl_syntax:clause(
    [erl_syntax:integer(CP)],
    none,
    [erl_syntax:atom("true")])};
is_letter_clause(_) ->
  false.

is_upper_clause(#ucd{cp = CP, gen_cat = "Lu"}) ->
  {true, erl_syntax:clause(
    [erl_syntax:integer(CP)],
    none,
    [erl_syntax:atom("true")])};
is_upper_clause(_) ->
  false.

is_lower_clause(#ucd{cp = CP, gen_cat = "Ll"}) ->
    {true, erl_syntax:clause(
      [erl_syntax:integer(CP)],
      none,
      [erl_syntax:atom("true")])};
is_lower_clause(_) ->
  false.

is_digit_clause(#ucd{cp = CP, gen_cat = [$N|_]}) ->
  {true, erl_syntax:clause(
    [erl_syntax:integer(CP)],
    none,
    [erl_syntax:atom("true")])};
is_digit_clause(_) ->
  false.
