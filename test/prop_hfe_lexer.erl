-module(prop_hfe_lexer).

-include("token.hrl").

-include_lib("proper/include/proper.hrl").
-include_lib("eunit/include/eunit.hrl").

scan_input(InputIoList, TokenType) ->
    Input = list_to_binary(InputIoList),
    Token = hd(hfe_lexer:tokenize(Input)),
    #token{type = TokenType, value = Input} =:= Token.

prop_lexes_integer() ->
    ?FORALL(N, non_neg_integer(),
        scan_input(io_lib:format("~p", [N]), integer)).

prop_lexes_float() ->
    ?FORALL(N, non_neg_float(),
        scan_input(io_lib:format("~p", [N]), float)).
