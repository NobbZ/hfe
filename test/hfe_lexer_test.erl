-module(hfe_lexer_test).

-include("token.hrl").
-include_lib("eunit/include/eunit.hrl").

all_test_() ->
    [ generic_test_gen(integer,  ["0", "1", "1000"])
    , generic_test_gen(float,    ["0.0", "1.0e1", "1.0e+1", "1.0e-1"])
    , generic_test_gen(lower_id, ["a", "aa", "aA", "a'", "a1", "a'a", "a1a'"])
    , generic_test_gen(upper_id, ["A", "Aa", "AA", "A'", "A1", "A'a", "A1a'"])
    , generic_test_gen(operator, ["!", "$", "%", "&", "/", "=", "?", "*", "+", "#", "-", "_", "<", ">", "|", "\\"])
    , generic_test_gen(operator, ["!!", "$$", "%%", "&&", "//", "==", "??", "**", "++", "##", "--", "__", "<<", ">>", "||", "\\\\"])
    , generic_test_gen(operator, ["!$", "$%", "%&", "&/", "/=", "=?", "?*", "*+", "+#", "#-", "-_", "_<", "<>", ">|", "|\\", "\\!"])
    , parens_and_brackets_gen([{"(", '('}, {")", ')'}, {"[", '['}, {"]", ']'}, {"{", '{'}, {"}", '}'}])
    ].

parens_and_brackets_gen(List) ->
    lists:map(fun({In, TT}) -> generic_test_gen(TT, [In]) end, List).

generic_test_gen(_, []) -> {generator, fun() -> [] end};
generic_test_gen(TT, [N|T]) ->
    {generator, fun() ->
        Input = list_to_binary(N),
        T1    = hd(hfe_lexer:tokenize(N)),
        T2    = hd(hfe_lexer:tokenize(Input)),
        [{lists:flatten(io_lib:format("~p-'~s'-list", [TT, N])), ?_assertMatch(#token{type = TT, value = Input}, T1)}
        ,{lists:flatten(io_lib:format("~p-'~s'-bnry", [TT, N])), ?_assertMatch(#token{type = TT, value = Input}, T2)}
        |generic_test_gen(TT, T)]
    end}.
