-record(token, {type, value}).

-type token() :: #token{
  type  :: token_type(),
  value :: binary()
}.

-type token_type() :: integer | float | '(' | ')' | '{' | '}' | '[' | ']' | lower_id | upper_id.